# Sentry Client

### About
Sentry-Client is a little utility framework designed to make integrating Sentry's error reporting into your apps and programs much easier.  
  
### Usage  
The simplies way to get Sentry-Client set up and ready is by using following snippet.  

```java
SentryClient.initialize();
SentryClient.getInstance().start(/* Your DSN here! */);

//Automatically sends uncaught exceptions. You can call SentryClient.getInstance().sendException() manually if you want to.
SentryClient.getInstance().setDefaultExceptionHandler();
```

Sentry Client also offers various "modules", which extend functionality of Sentry-Client.  
  
##### Sentry Report Manager Module  
This module saves unsent events/reports for you if they fail to send the first time.  
To use this just simply add following lines into SentryClient's initialization above.  
  
```java
SentryClient.getInstance().setSentryReportManager(
    new DefaultSentryReportManager("folder/to/store/events/in")
);
```
  
DefaultSentryReportManager class uses Java's serialization to serialize reports/events.  
If you wish to use different kind of serialization, you can make your own implementation of SentryReportManager.  
  
###### Sentry Logger Module  
This module saves last log entries and adds them to reports/events as breadcrumbs.  

**WARNING**  
Be careful as this module doesn't work together with DefaultSentryReportManager,  
because you can't serialize Breadcrumb class at the moment.  
This gets fixed as soon as following raven-java PR (https://github.com/getsentry/raven-java/pull/275) is merged and new version is released.  
**-------**
  
To use logger module just add following lines into Sentry-Client's initialization and call onXXXXLogged methods when using your logger.  

```java
//Sentry Initialization
SentryClient.getInstance().setSentryLogger(new SentryLogger(10 /*Send 10 last log entries*/);

//In your logger class
public static void logDebug(String tag, String message) { //Example method
    //Do your logging
    
    if(SentryClient.isRunning() && SentryClient.getInstance().hasSentryLogger())
			SentryClient.getInstance().getSentryLogger().onDebugLogged(tag, message);
}
```





