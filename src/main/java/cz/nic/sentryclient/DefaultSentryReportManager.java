package cz.nic.sentryclient;

import com.getsentry.raven.event.Event;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.InvalidClassException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by drahomir on 11/11/16.
 */
public class DefaultSentryReportManager extends SentryReportManager {
    private static final String  DEFAULT_EVENT_FILE_EXTENSION = ".err";
    private static final String  HIDDEN_FILE_PREFIX           = ".";

    private static final boolean DEFAULT_HIDE_FILES           = false;

    private String  folderPath;
    private String  fileExtension;
    private boolean hideEventFiles;

    public DefaultSentryReportManager(String folderPath) {
        this(folderPath, DEFAULT_HIDE_FILES);
    }

    public DefaultSentryReportManager(String folderPath, boolean hideEventFiles) {
        this(folderPath, DEFAULT_EVENT_FILE_EXTENSION, hideEventFiles);
    }

    public DefaultSentryReportManager(String folderPath, String fileExtension, boolean hideEventFiles) {
        this.folderPath = folderPath;
        this.fileExtension = fileExtension;
        this.hideEventFiles = hideEventFiles;
    }

    @Override
    void storeEvent(Event event) {
        try {
            String fileName  = (hideEventFiles ? HIDDEN_FILE_PREFIX : "") + event.getId().toString() + fileExtension;

            File file = new File(folderPath + File.separator + fileName);
            if(file.exists()) {
                file.delete();
            }

            file.getParentFile().mkdirs();
            file.createNewFile();

            FileOutputStream fileOut = new FileOutputStream(file);;
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOut);
            objectOutputStream.writeObject(event);
            objectOutputStream.close();
            fileOut.close();
            objectOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
    }

    @Override
    List<Event> getStoredEvents() {
        List<Event> events = new ArrayList<Event>();

        try {
            File dir = new File(folderPath);
            if(!dir.exists()) return events;

            File[] files = dir.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File file, String s) {
                    return s.endsWith(fileExtension);
                }
            });

            for(File file : files) {
                Event e = deserializeEvent(file);
                if(e != null) {
                    events.add(e);
                }
            }

            return events;
        }
        catch (Exception e) {
            e.printStackTrace();
            return events;
        }
    }

    private Event deserializeEvent(File file) {
        Event event = null;

        try {
            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);

            event = (Event) ois.readObject();
            ois.close();
            fis.close();
        } catch (InvalidClassException e) {
            file.delete();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return event;
    }
}