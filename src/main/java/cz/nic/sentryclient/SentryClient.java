package cz.nic.sentryclient;

import com.getsentry.raven.DefaultRavenFactory;
import com.getsentry.raven.Raven;
import com.getsentry.raven.connection.Connection;
import com.getsentry.raven.connection.EventSendFailureCallback;
import com.getsentry.raven.dsn.Dsn;
import com.getsentry.raven.dsn.InvalidDsnException;
import com.getsentry.raven.event.Event;
import com.getsentry.raven.event.EventBuilder;
import com.getsentry.raven.event.interfaces.ExceptionInterface;

public class SentryClient {
    private static class RavenFactoryEnhanced extends DefaultRavenFactory {
        private EventSendFailureCallback eventSendFailureCallback;

        public Raven ravenInstance(String DSN, EventSendFailureCallback callback) {
            eventSendFailureCallback = callback;
            return createRavenInstance(new Dsn(DSN));
        }

        @Override
        protected Connection createConnection(Dsn dsn) {
            Connection connection = super.createConnection(dsn);

            if(eventSendFailureCallback != null) connection.addEventSendFailureCallback(eventSendFailureCallback);
            return connection;
        }
    }

    private static final RavenFactoryEnhanced     RAVEN_FACTORY_ENHANCED     = new RavenFactoryEnhanced();

    private static final String                   NOT_INITIALIZED_MESSAGE    = "SentryClient is not initialized. Did you forget to call SentryClient.initialize() method?";
    private static final String                   NOT_RUNNING_MESSAGE        = "SentryClient is not running. Did you forget to call SentryClient.start() method?";

    private final Thread.UncaughtExceptionHandler UNCAUGHT_EXCEPTION_HANDLER = new Thread.UncaughtExceptionHandler() {
        @Override
        public void uncaughtException(Thread thread, Throwable throwable) {
            throwable.printStackTrace();
            if(instance != null && instance.raven != null) sendException(throwable);
        }
    };

    private final EventSendFailureCallback        EVENT_SEND_FAILURE_CALLBACK = new EventSendFailureCallback() {
        @Override
        public void onFailure(Event event, Exception exception) {
            if(sentryReportManager != null) {
                sentryReportManager.getEventSendFailureCallback().onFailure(event, exception);
            }
        }
    };

    private static SentryClient        instance;

    private        SentryReportManager sentryReportManager;
    private        SentryLogger        sentryLogger;
    private        SentryHelper        sentryHelper;
    private        boolean             sentryHelperActive;
    private        Raven               raven;

    private        String              applicationVersion;

    //Singleton
    private SentryClient() {
        sentryHelperActive = false;
    }

    /**
     * @return true if SentryClient was initialized successfully and false if SentryClient was already initialized before...
     */
    public static boolean initialize() {
        if(instance != null) return false;

        instance = new SentryClient();
        return true;
    }

    public static SentryClient getInstance() {
        if(instance == null) throw new RuntimeException(NOT_INITIALIZED_MESSAGE);
        return instance;
    }

    public void setSentryReportManager(SentryReportManager sentryReportManager) {
        if(sentryReportManager != null) {
            this.sentryReportManager = sentryReportManager;
            this.sentryReportManager.setSentryClient(instance);
        }
    }

    public SentryReportManager getSentryReportManager() {
        return sentryReportManager;
    }

    public boolean hasSentryReportManager() {
        return sentryReportManager != null;
    }

    public void setSentryLogger(SentryLogger sentryLogger) {
        if(sentryLogger != null) {
            this.sentryLogger = sentryLogger;
        }
    }

    public SentryLogger getSentryLogger() {
        return sentryLogger;
    }

    public boolean hasSentryLogger() {
        return sentryLogger != null;
    }

    public void setSentryHelper(SentryHelper sentryHelper) {
        this.sentryHelper = sentryHelper;
    }

    public SentryHelper getSentryHelper() {
        return sentryHelper;
    }

    public boolean hasSentryHelper() {
        return sentryHelper != null;
    }

    public void setDefaultExceptionHandler() {
        Thread.setDefaultUncaughtExceptionHandler(UNCAUGHT_EXCEPTION_HANDLER);
    }

    public void setApplicationVersion(String appVersion) {
        this.applicationVersion = appVersion;
    }

    public void start(String DSN) {
        if(DSN == null) throw new InvalidDsnException("DSN is null!");
        this.raven = RAVEN_FACTORY_ENHANCED.ravenInstance(DSN, EVENT_SEND_FAILURE_CALLBACK);
    }

    public static boolean isInitialized() {
        return instance != null;
    }

    public static boolean isRunning() {
        return isInitialized() && instance.raven != null;
    }

    public void sendCustomMessage(Class clazz, String message, Event.Level level) {
        EventBuilder eventBuilder = new EventBuilder();
        eventBuilder.withMessage(clazz + ": " + message);
        eventBuilder.withLevel(level);

        prepareAndSendEvent(eventBuilder);
    }

    public void sendException(Throwable throwable) {
        if(raven == null) throw new RuntimeException(NOT_RUNNING_MESSAGE);

        EventBuilder eventBuilder = new EventBuilder();
        eventBuilder.withSentryInterface(new ExceptionInterface(throwable));
        eventBuilder.withMessage(throwable.getMessage());
        eventBuilder.withLevel(Event.Level.ERROR);

        prepareAndSendEvent(eventBuilder);
    }

    private void prepareAndSendEvent(EventBuilder eventBuilder) {
        if(applicationVersion != null) eventBuilder.withRelease(applicationVersion);

        if(hasSentryHelper() && !sentryHelperActive) {
            raven.addBuilderHelper(sentryHelper);
            sentryHelperActive = true;
        }
        raven.runBuilderHelpers(eventBuilder);

        if(hasSentryLogger()) {
            eventBuilder.withBreadcrumbs(sentryLogger.getBreadCrumbs());
        }

        sendEvent(eventBuilder.build());
    }

    public void sendEvent(Event event) {
        if(raven == null) throw new RuntimeException(NOT_RUNNING_MESSAGE);

        if(event != null) raven.sendEvent(event);
    }
}