package cz.nic.sentryclient;

import com.getsentry.raven.event.EventBuilder;
import com.getsentry.raven.event.helper.EventBuilderHelper;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by drahomir on 11/14/16.
 */
public class SentryHelper implements EventBuilderHelper {
    private static final String NULL_VALUE = "NULL";

    private HashMap<String, String> tags;
    private HashMap<String, String> extras;

    public SentryHelper() {
        this.tags = new HashMap<String, String>();
        this.extras = new HashMap<String, String>();
    }

    public void addTag(String tagKey, String tagValue) {
        if(tagKey != null) tags.put(tagKey, tagValue == null ? NULL_VALUE : tagValue);
    }

    public void removeTag(String tagKey) {
        if(tags.containsKey(tagKey)) tags.remove(tagKey);
    }

    public void addExtra(String extraKey, String extraValue) {
        if(extraKey != null) extras.put(extraKey, extraValue == null ? NULL_VALUE : extraValue);
    }

    public void removeExtra(String extraKey) {
        if(extras.containsKey(extraKey)) extras.remove(extraKey);
    }

    @Override
    public void helpBuildingEvent(EventBuilder eventBuilder) {
        for(Map.Entry<String, String> tag : tags.entrySet()) {
            eventBuilder.withTag(tag.getKey(), tag.getValue());
        }

        for(Map.Entry<String, String> extra : extras.entrySet()) {
            eventBuilder.withExtra(extra.getKey(), extra.getValue());
        }
    }
}
