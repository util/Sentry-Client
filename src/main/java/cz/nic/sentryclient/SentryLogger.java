package cz.nic.sentryclient;

import com.getsentry.raven.event.Breadcrumb;
import com.getsentry.raven.event.BreadcrumbBuilder;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class SentryLogger {
    private final int size;

    public enum SentryLogEntryType {
        DEFAULT("default"),
        HTTP("http"),
        NAVIGATION("navigation");

        private final String type;

        SentryLogEntryType(String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }
    }

    public enum SentryLogEntryLevel {
        DEBUG(    0, "debug"),
        INFO(     1, "info"),
        WARNING(  2, "warning"),
        ERROR(    3, "error"),
        CRITICAL( 4, "critical"),
        NONE(    10, "none");

        private final int level;
        private final String levelName;

        SentryLogEntryLevel(int level, String levelName) {
            this.level = level;
            this.levelName = levelName;
        }

        public int getLevel() {
            return level;
        }

        public String getLevelName() {
            return levelName;
        }
    }

    private class SentryLoggerEntry {
        private Date                time;
        private SentryLogEntryType  type;
        private String              message;
        private String              category;
        private SentryLogEntryLevel level;

        public SentryLoggerEntry(Date time, SentryLogEntryType type, String message, String category, SentryLogEntryLevel level) {
            this.time = time;
            this.type = type;
            this.message = message;
            this.category = category;
            this.level = level;
        }
    }

    private LinkedList<SentryLoggerEntry> loggedEntries;
    private SentryLogEntryLevel           loggingLevel;

    public SentryLogger(int size) {
        this(size, SentryLogEntryLevel.DEBUG);
    }

    public SentryLogger(int size, SentryLogEntryLevel loggingLevel) {
        this.size = size;
        this.loggingLevel = loggingLevel;
        this.loggedEntries = new LinkedList<SentryLoggerEntry>();
    }

    public void onDebugLogged(String category, String message) {
        addLogEntry(category, message, SentryLogEntryLevel.DEBUG);
    }

    public void onInfoLogged(String category, String message) {
        addLogEntry(category, message, SentryLogEntryLevel.INFO);
    }

    public void onWarningLogged(String category, String message) {
        addLogEntry(category, message, SentryLogEntryLevel.WARNING);
    }

    public void onErrorLogged(String category, String message) {
        addLogEntry(category, message, SentryLogEntryLevel.ERROR);
    }

    public void onCriticalLogged(String category, String message) {
        addLogEntry(category, message, SentryLogEntryLevel.CRITICAL);
    }

    private void addLogEntry(String category, String message, SentryLogEntryLevel level) {
        if(level.getLevel() < loggingLevel.getLevel()) return;

        if(loggedEntries.size() >= size) loggedEntries.removeFirst();
        loggedEntries.addLast(new SentryLoggerEntry(new Date(), SentryLogEntryType.DEFAULT, message, category, level));
    }

    public List<Breadcrumb> getBreadCrumbs() {
        BreadcrumbBuilder breadcrumbBuilder = new BreadcrumbBuilder();

        List<Breadcrumb> breadcrumbs = new ArrayList<Breadcrumb>(loggedEntries.size());

        for(SentryLoggerEntry entry : loggedEntries) {
            breadcrumbBuilder
                    .setTimestamp(entry.time)
                    .setType(entry.type.getType())
                    .setMessage(entry.message)
                    .setCategory(entry.category)
                    .setLevel(entry.level.getLevelName());

            breadcrumbs.add(breadcrumbBuilder.build());
        }

        return breadcrumbs;
    }
}