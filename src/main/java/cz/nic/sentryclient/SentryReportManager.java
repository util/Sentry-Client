package cz.nic.sentryclient;

/**
 * Created by drahomir on 11/11/16.
 */

import com.getsentry.raven.connection.EventSendFailureCallback;
import com.getsentry.raven.event.Event;

import java.util.List;

/**
 * Stored and Restores events
 */
public abstract class SentryReportManager {
    private EventSendFailureCallback eventSendFailureCallback;
    private SentryClient sentryClient;

    SentryReportManager() {
        eventSendFailureCallback = new EventSendFailureCallback() {
            @Override
            public void onFailure(Event event, Exception exception) {
                storeEvent(event);
            }
        };
    }

    public void sendStoredEvents() {
        List<Event> storedEvents = getStoredEvents();

        for(Event event : storedEvents) {
            sentryClient.sendEvent(event);
        }
    }

    protected void setSentryClient(SentryClient sentryClient) {
        this.sentryClient = sentryClient;
    }

    public EventSendFailureCallback getEventSendFailureCallback() {
        return eventSendFailureCallback;
    }

    abstract void storeEvent(Event event);

    /**
     * Gets List of already stored events.
     * @returns null if there are no stored events.
     */
    abstract List<Event> getStoredEvents();
}